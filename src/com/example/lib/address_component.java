package com.example.lib;

import java.util.List;

public class address_component {
	String long_name="";
	String short_name="";
	List<type> type=null;
	
	public String getLong_name() {
		return long_name;
	}
	public void setLong_name(String long_name) {
		this.long_name = long_name;
	}
	public String getShort_name() {
		return short_name;
	}
	public void setShort_name(String short_name) {
		this.short_name = short_name;
	}
	public List<type> getType() {
		return type;
	}
	public void setType(List<type> type) {
		this.type = type;
	}
	

}
