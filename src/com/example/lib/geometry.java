package com.example.lib;

import java.util.List;

public class geometry {
	List<latlng> location=null;
	String location_type="";
	List<sn> viewport=null;
	List<sn> bounds=null;
	List<latlng> southwest=null;
	List<latlng> northeast=null;
	public List<latlng> getLocation() {
		return location;
	}
	public void setLocation(List<latlng> location) {
		this.location = location;
	}
	public String getLocation_type() {
		return location_type;
	}
	public void setLocation_type(String location_type) {
		this.location_type = location_type;
	}
	public List<sn> getViewport() {
		return viewport;
	}
	public void setViewport(List<sn> viewport) {
		this.viewport = viewport;
	}
	public List<sn> getBounds() {
		return bounds;
	}
	public void setBounds(List<sn> bounds) {
		this.bounds = bounds;
	}
	public List<latlng> getSouthwest() {
		return southwest;
	}
	public void setSouthwest(List<latlng> southwest) {
		this.southwest = southwest;
	}
	public List<latlng> getNortheast() {
		return northeast;
	}
	public void setNortheast(List<latlng> northeast) {
		this.northeast = northeast;
	}


}
