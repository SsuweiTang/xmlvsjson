package com.example.lib;


import java.util.List;

public class map {
	
	String formatted_address="";
	List<address_component> addr=null;
	List<geometry> geo=null;
	List<type> type=null;
	
	public String getFormatted_address() {
		return formatted_address;
	}
	public void setFormatted_address(String formatted_address) {
		this.formatted_address = formatted_address;
	}
	public List<address_component> getAddr() {
		return addr;
	}
	public void setAddr(List<address_component> addr) {
		this.addr = addr;
	}
	public List<geometry> getGeo() {
		return geo;
	}
	public void setGeo(List<geometry> geo) {
		this.geo = geo;
	}
	public List<type> getType() {
		return type;
	}
	public void setType(List<type> type) {
		this.type = type;
	}
	

}
