package com.example.xmlvsjson;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import com.example.lib.address_component;
import com.example.lib.geometry;
import com.example.lib.latlng;
import com.example.lib.map;
import com.example.lib.sn;
import com.example.lib.type;

public class DOM {

	public DOM() {
		// TODO Auto-generated constructor stub
	}

	public List<map> getParser(String input) {

		List<map> list_map = null;
		List<type> list_type = null;
		List<geometry> list_geometry = null;
		List<latlng> list_location = null;
		List<latlng> list_southwest = null;
		List<latlng> list_northeast = null;
		List<address_component> list_addr = null;
		List<type> list_Type = null;
		List<latlng> list_southwest_v = null;
		List<latlng> list_northeast_v = null;
		List<latlng> list_southwest_b = null;
		List<latlng> list_northeast_b = null;
		List<sn> list_viewport = null;
		List<sn> list_bounds = null;
		type t = null;
		type T = null;
		geometry geo = null;
		address_component addr = null;
		latlng L = null;
		latlng s = null;
		latlng n = null;
		latlng vs = null;
		latlng vn = null;
		latlng bs = null;
		latlng bn = null;
		sn view = null;
		sn bound = null;
		map m = null;
		try {
			URL url = new URL(input.toString());
			InputStream is = url.openStream();
			DocumentBuilderFactory factory = DocumentBuilderFactory	.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(is, "utf-8");
			Element element = document.getDocumentElement();
			list_map = new ArrayList<map>();
			NodeList nodes = element.getElementsByTagName("result");
			for (int i = 0; i < nodes.getLength(); i++) {
				Element subElement = (Element) nodes.item(i);
				list_addr = new ArrayList<address_component>();
				list_geometry = new ArrayList<geometry>();
				m = new map();
				list_Type = new ArrayList<type>();
				NodeList childNodes = subElement.getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					if (childNodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
						if ((childNodes.item(j).getNodeName()).equals("type")) {
							T = new type();
							T.setType(childNodes.item(j).getTextContent());
							list_Type.add(T);
							T = null;
						}
						if ((childNodes.item(j).getNodeName()).equals("formatted_address")) {
							m.setFormatted_address(childNodes.item(j).getTextContent());
							if ((childNodes.item(j).getNodeName()).equals("address_component")) {
								NodeList childNodeschild = childNodes.item(j).getChildNodes();
								list_type = new ArrayList<type>();
								addr = new address_component();
								for (int k = 0; k < childNodeschild.getLength(); k++) {									
									if (childNodeschild.item(k).getNodeType() == Node.ELEMENT_NODE) {
										if ((childNodeschild.item(k).getNodeName())	.equals("long_name")) {
											addr.setLong_name(childNodeschild.item(k).getTextContent());
										}
										if ((childNodeschild.item(k).getNodeName()).equals("short_name")) {
											addr.setShort_name(childNodeschild.item(k).getTextContent());
										}
									}
									if ((childNodeschild.item(k).getNodeName()).equals("type")) {
										t = new type();
										t.setType(childNodeschild.item(k).getTextContent());
									}
								}
								addr.setType(list_type);
								list_addr.add(addr);
								addr = null;
							}
						}
						if ((childNodes.item(j).getNodeName()).equals("geometry")) {
							geo = new geometry();
							list_bounds = new ArrayList<sn>();
							list_viewport = new ArrayList<sn>();
							NodeList childNodeschild = childNodes.item(j).getChildNodes();
							for (int k = 0; k < childNodeschild.getLength(); k++) {
								if ((childNodeschild.item(k).getNodeName()).equals("location")) {
									list_location = new ArrayList<latlng>();
									L = new latlng();
									NodeList subchilechildNodeschild = childNodeschild.item(k).getChildNodes();
									for (int l = 0; l < subchilechildNodeschild	.getLength(); l++) {
										if ((subchilechildNodeschild.item(l).getNodeName()).equals("lat")) {
											L.setLat(subchilechildNodeschild.item(l).getTextContent());
										}
										if ((subchilechildNodeschild.item(l).getNodeName()).equals("lng")) {
											L.setLng(subchilechildNodeschild.item(l).getTextContent());
										}
									}
									list_location.add(L);
									L = null;
								}
								if ((childNodeschild.item(k).getNodeName())	.equals("location_type")) {
									geo.setLocation_type(childNodeschild.item(k).getTextContent());
								}
								if ((childNodeschild.item(k).getNodeName())	.equals("bounds")) {
									bound = new sn();
									NodeList chilechildNodeschild = childNodeschild	.item(k).getChildNodes();
									for (int p = 0; p < chilechildNodeschild.getLength(); p++) {
										if ((chilechildNodeschild.item(p).getNodeName()).equals("southwest")) {
											list_southwest_b = new ArrayList<latlng>();
											bs = new latlng();
											NodeList subchilechildNodeschild = chilechildNodeschild	.item(p).getChildNodes();
											for (int l = 0; l < subchilechildNodeschild	.getLength(); l++) {
												if ((subchilechildNodeschild.item(l).getNodeName()).equals("lat")) {
													bs.setLat(subchilechildNodeschild.item(l).getTextContent());
												}
												if ((subchilechildNodeschild.item(l).getNodeName())	.equals("lng")) {
													bs.setLng(subchilechildNodeschild.item(l).getTextContent());
												}
											}
											list_southwest_b.add(bs);
											bound.setSouthwest(list_southwest_b);
										}
										if ((chilechildNodeschild.item(p).getNodeName()).equals("northeast")) {
											list_northeast_b = new ArrayList<latlng>();
											bn = new latlng();
											NodeList subchilechildNodeschild = chilechildNodeschild	.item(p).getChildNodes();
											for (int l = 0; l < subchilechildNodeschild	.getLength(); l++) {
												if ((subchilechildNodeschild.item(l).getNodeName()).equals("lat")) {
													bn.setLat(subchilechildNodeschild.item(l).getTextContent());
												}
												if ((subchilechildNodeschild.item(l).getNodeName()).equals("lng")) {
													bn.setLng(subchilechildNodeschild.item(l).getTextContent());
												}
											}
											list_northeast_b.add(bn);
											bound.setNortheast(list_northeast_b);
										}
									}
									list_bounds.add(bound);
								}
								if ((childNodeschild.item(k).getNodeName())	.equals("viewport")) {
									view = new sn();
									NodeList chilechildNodeschild = childNodeschild	.item(k).getChildNodes();
									for (int p = 0; p < chilechildNodeschild.getLength(); p++) {
										if ((chilechildNodeschild.item(p).getNodeName()).equals("southwest")) {
											list_southwest_v = new ArrayList<latlng>();
											vs = new latlng();
											NodeList subchilechildNodeschild = chilechildNodeschild	.item(p).getChildNodes();
											for (int l = 0; l < subchilechildNodeschild.getLength(); l++) {
												if ((subchilechildNodeschild.item(l).getNodeName())	.equals("lat")) {
													vs.setLat(subchilechildNodeschild.item(l).getTextContent());
												}
												if ((subchilechildNodeschild.item(l).getNodeName()).equals("lng")) {
													vs.setLng(subchilechildNodeschild.item(l).getTextContent());
												}
											}
											list_southwest_v.add(vs);
											view.setSouthwest(list_southwest_v);
										}
										if ((chilechildNodeschild.item(p).getNodeName()).equals("northeast")) {
											list_northeast_v = new ArrayList<latlng>();
											vn = new latlng();
											NodeList subchilechildNodeschild = chilechildNodeschild.item(p).getChildNodes();
											for (int l = 0; l < subchilechildNodeschild.getLength(); l++) {
												if ((subchilechildNodeschild.item(l).getNodeName()).equals("lat")) {
													vn.setLat(subchilechildNodeschild.item(l).getTextContent());
												}
												if ((subchilechildNodeschild.item(l).getNodeName()).equals("lng")) {
													vn.setLng(subchilechildNodeschild.item(l).getTextContent());
												}
											}
											list_northeast_v.add(vn);
											view.setNortheast(list_northeast_v);
										}
									}
									list_viewport.add(view);
								}
							}
							geo.setLocation(list_location);
							geo.setBounds(list_bounds);
							geo.setViewport(list_viewport);
							list_geometry.add(geo);
							geo = null;
						}
					}
				}
				m.setAddr(list_addr);
				m.setGeo(list_geometry);
				m.setType(list_Type);
				list_map.add(m);
				m = null;
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		return list_map;
	}
}
