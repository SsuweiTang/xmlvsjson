package com.example.xmlvsjson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.w3c.dom.NodeList;

import com.example.lib.address_component;
import com.example.lib.geometry;
import com.example.lib.latlng;
import com.example.lib.map;
import com.example.lib.sn;
import com.example.lib.type;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.widget.TextView;

public class JSON {

	List<map> list_map = null;
	List<type> list_type = null;
	List<geometry> list_geometry = null;
	List<latlng> list_location = null;
	List<latlng> list_southwest = null;
	List<latlng> list_northeast = null;
	List<address_component> list_addr = null;
	List<type> list_Type = null;
	List<latlng> list_southwest_v = null;
	List<latlng> list_northeast_v = null;
	List<latlng> list_southwest_b = null;
	List<latlng> list_northeast_b = null;
	List<sn> list_viewport = null;
	List<sn> list_bounds = null;
	type t = null;
	type T = null;
	geometry geo = null;
	address_component addr = null;
	latlng L = null;
	latlng s = null;
	latlng n = null;
	latlng vs = null;
	latlng vn = null;
	latlng bs = null;
	latlng bn = null;
	sn view = null;
	sn bound = null;
	map m = null;

	public JSON() {
	}
	public List<map> getParser(String input) {
		JSONObject jsonRootObject;
		try {

			jsonRootObject = new JSONObject(getJSONUrl(input));
			JSONArray jsonArray = jsonRootObject.optJSONArray("results");
			list_map = new ArrayList<map>();
			for (int i = 0; i < jsonArray.length(); i++) {
				list_addr = new ArrayList<address_component>();
				list_geometry = new ArrayList<geometry>();
				m = new map();
				list_Type = new ArrayList<type>();
				JSONObject jsonObject;
				jsonObject = jsonArray.getJSONObject(i);
				m.setFormatted_address(jsonObject.optString("formatted_address").toString());
				JSONArray jArray1 = jsonObject.getJSONArray("types");
				for (int n = 0; n < jArray1.length(); n++) {
					T = new type();
					T.setType(jArray1.getString(n));
					list_Type.add(T);
				}
				JSONArray jsonArraySub1 = jsonObject.optJSONArray("address_components");
				for (int j = 0; j < jsonArraySub1.length(); j++) {
					list_type = new ArrayList<type>();
					addr = new address_component();
					JSONObject jsonObject2 = jsonArraySub1.getJSONObject(j);
					addr.setLong_name(jsonObject2.optString("long_name").toString());
					addr.setShort_name(jsonObject2.optString("short_name").toString());
					JSONArray jArray2 = jsonObject2.getJSONArray("types");
					for (int n = 0; n < jArray2.length(); n++) {
						t = new type();
						t.setType(jArray2.getString(n));
						list_type.add(t);
					}
					addr.setType(list_type);
					list_addr.add(addr);
				}
				JSONObject jsonObject3 = jsonObject.optJSONObject("geometry");
				geo = new geometry();
				list_bounds = new ArrayList<sn>();
				list_viewport = new ArrayList<sn>();	
				JSONObject jsonObject4 = jsonObject3.optJSONObject("location");
				list_location = new ArrayList<latlng>();
				L = new latlng();
				L.setLat(jsonObject4.optString("lat").toString());
				L.setLng(jsonObject4.optString("lng").toString());
				list_location.add(L);			
				geo.setLocation_type(jsonObject3.optString("location_type")	.toString());
				JSONObject jsonObject5 = jsonObject3.optJSONObject("viewport");
				view = new sn();	
				JSONObject jsonObject6 = jsonObject5.optJSONObject("northeast");
				list_northeast_v = new ArrayList<latlng>();
				vn = new latlng();
				JSONObject jsonObject7 = jsonObject5.optJSONObject("southwest");
				list_southwest_v = new ArrayList<latlng>();
				vs = new latlng();
				vs.setLat(jsonObject7.optString("lat").toString());
				vs.setLng(jsonObject7.optString("lng").toString());
				list_southwest_v.add(vs);
				view.setSouthwest(list_southwest_v);
				vn.setLat(jsonObject6.optString("lat").toString());
				vn.setLng(jsonObject6.optString("lng").toString());
				list_northeast_v.add(vn);
				view.setNortheast(list_northeast_v);
				list_viewport.add(view);
				if (jsonObject3.optJSONObject("bounds") != null) {
					JSONObject jsonObject8 = jsonObject3.optJSONObject("bounds");
					bound = new sn();
					JSONObject jsonObject9 = jsonObject8.optJSONObject("northeast");
					list_northeast_b = new ArrayList<latlng>();
					bn = new latlng();

					JSONObject jsonObject10 = jsonObject8.optJSONObject("southwest");
					list_southwest_b = new ArrayList<latlng>();
					bs = new latlng();

					bs.setLat(jsonObject10.optString("lat").toString());
					bs.setLng(jsonObject10.optString("lng").toString());
					list_southwest_b.add(bs);
					bound.setSouthwest(list_southwest_b);

					bn.setLat(jsonObject9.optString("lat").toString());
					bn.setLng(jsonObject9.optString("lng").toString());
					list_northeast_b.add(bn);
					bound.setNortheast(list_northeast_b);
					list_bounds.add(bound);
				}

				geo.setLocation(list_location);
				geo.setBounds(list_bounds);
				geo.setViewport(list_viewport);
				list_geometry.add(geo);
				geo = null;
				m.setAddr(list_addr);
				m.setGeo(list_geometry);
				m.setType(list_Type);
				list_map.add(m);
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return list_map;
	}

	private String getJSONUrl(String url) {
		StringBuilder str = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) { // Download OK
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					str.append(line);
				}
			} else {
				Log.e("Log", "Failed to download result..");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str.toString();
	}

}
