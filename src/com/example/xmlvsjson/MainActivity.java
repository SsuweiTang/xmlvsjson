package com.example.xmlvsjson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {
	Thread threadjson = null;
	TextView tv_json=null;
	TextView tv_json_time=null;
	String str_json="";
	List<MapContent> list = new ArrayList<MapContent>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tv_json_time=(TextView)findViewById(R.id.textView1);
		tv_json=(TextView)findViewById(R.id.textView2);
		
		jsonparser();
	}

	private void jsonparser() {
		// TODO Auto-generated method stub
		String url = "http://maps.google.com.tw/maps/api/geocode/json?latlng=24.097305,120.6848459&sensor=true";
		JSONObject jsonRootObject;
		MapContent map=null;
		 long beforeTime = System.currentTimeMillis();
		try {
			list = new ArrayList<MapContent>();
			jsonRootObject = new JSONObject(getJSONUrl(url));
			JSONArray jsonArray = jsonRootObject.optJSONArray("results");
			

			for (int i = 0; i < jsonArray.length(); i++) {
				
				 map = new MapContent();
				JSONObject jsonObject;

				jsonObject = jsonArray.getJSONObject(i);
				String formatted_address = jsonObject.optString("formatted_address").toString();
//				String[] types= jsonObject.optJSONArray("types").toString();
				String types= jsonObject.optJSONArray("types").toString();
				map.setFormatted_address(jsonObject.optString("formatted_address").toString());
//				map.setType(types);
				JSONArray jArray3 = jsonObject.getJSONArray("types");
				
				for(int n = 0; n < jArray3.length(); n++)
				{
					JSONObject object3 = jArray3.getJSONObject(i);
					String types333 = object3.optString("types").toString();
					str_json += "<types333> " + types333 + " \n <types333> ";				
				}
				
				 str_json += "Node" + i + " : \n ";
				  
				 str_json += "<formatted_address> " + formatted_address + " \n <types> "
							+ types + "\n ";
				 str_json += "<types> " + formatted_address + " \n <types> "
							+ types + "\n ";
			
				JSONArray jsonArraySub1 = jsonObject.optJSONArray("address_components");
				for (int j = 0; j < jsonArraySub1.length(); j++) {
					 JSONObject jsonObject2 = jsonArraySub1.getJSONObject(j);
					 String  short_name = jsonObject2.optString("short_name").toString();
					 
					 String  long_name= jsonObject2.optString("long_name").toString();
					  
					 JSONArray jsonArraySub1Sub1=jsonObject2.optJSONArray("types");
					 
					 
					 str_json += "     <short_name>" + short_name + "\n "
								+ "    <long_name>" + long_name + "\n "
								+ "    <types>" + jsonArraySub1Sub1 + "\n ";
					 

				}
				
				
				
				JSONObject jsonObject3 = jsonObject.optJSONObject("geometry");
				JSONObject jsonObject4 = jsonObject3.optJSONObject("location");
				String lat = jsonObject4.optString("lat").toString();
				String lng = jsonObject4.optString("lng").toString();
				String location_type = jsonObject3.optString("location_type").toString();

				str_json += "   <location_type> "+ location_type + "\n ";
				str_json += "   <lat> "+ lat + "\n ";
				str_json += "   <lng> "+ lng + "\n ";
				JSONObject jsonObject5 = jsonObject3.optJSONObject("viewport");
				JSONObject jsonObject6 = jsonObject5.optJSONObject("northeast");
				String lat2 = jsonObject6.optString("lat").toString();
				String lng2 = jsonObject6.optString("lng").toString();
				
				str_json += "   <lat> "+ lat2 + "\n ";
				str_json += "   <lng> "+ lng2 + "\n ";
				
					 
				str_json += " <types> "
						+ types + "\n ";
				
				list.add(map);
			}
			
			long afterTime = System.currentTimeMillis();
			long parseTime = (afterTime-beforeTime);
			tv_json_time.setText("time"+parseTime);
			tv_json.setText(str_json);

		}

		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		
	}

	private String getJSONUrl(String url) {
		StringBuilder str = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) { // Download OK
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					str.append(line);
				}
			} else {
				Log.e("Log", "Failed to download result..");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str.toString();
	}

}
