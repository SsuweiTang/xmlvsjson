package com.example.xmlvsjson;

public class MapContent {
	private String[] type = null;
	private String formatted_address = "";
	private String[] address_component = null;
	private String geometry = "";
	private String location_lat = "";
	private String location_lng = "";
	private String location_type = "";
	private String southwest_lat = "";
	private String southwest_lng = "";
	private String northeast_lat = "";
	private String northeast_lng = "";
	public String[] getType() {
		return type;
	}
	public void setType(String[] type) {
		this.type = type;
	}
	public String getFormatted_address() {
		return formatted_address;
	}
	public void setFormatted_address(String formatted_address) {
		this.formatted_address = formatted_address;
	}
	public String[] getAddress_component() {
		return address_component;
	}
	public void setAddress_component(String[] address_component) {
		this.address_component = address_component;
	}
	public String getGeometry() {
		return geometry;
	}
	public void setGeometry(String geometry) {
		this.geometry = geometry;
	}
	public String getLocation_lat() {
		return location_lat;
	}
	public void setLocation_lat(String location_lat) {
		this.location_lat = location_lat;
	}
	public String getLocation_lng() {
		return location_lng;
	}
	public void setLocation_lng(String location_lng) {
		this.location_lng = location_lng;
	}
	public String getLocation_type() {
		return location_type;
	}
	public void setLocation_type(String location_type) {
		this.location_type = location_type;
	}
	public String getSouthwest_lat() {
		return southwest_lat;
	}
	public void setSouthwest_lat(String southwest_lat) {
		this.southwest_lat = southwest_lat;
	}
	public String getSouthwest_lng() {
		return southwest_lng;
	}
	public void setSouthwest_lng(String southwest_lng) {
		this.southwest_lng = southwest_lng;
	}
	public String getNortheast_lat() {
		return northeast_lat;
	}
	public void setNortheast_lat(String northeast_lat) {
		this.northeast_lat = northeast_lat;
	}
	public String getNortheast_lng() {
		return northeast_lng;
	}
	public void setNortheast_lng(String northeast_lng) {
		this.northeast_lng = northeast_lng;
	}
	
	
	
	
			

}
