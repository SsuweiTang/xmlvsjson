package com.example.xmlvsjson;



import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.example.lib.address_component;
import com.example.lib.geometry;
import com.example.lib.latlng;
import com.example.lib.map;
import com.example.lib.sn;
import com.example.lib.type;

public class PULL {

	public PULL() {
	}

	public List<map> getParser(String input)  {

		
		 List<map> list_map = null;
		 List<type> list_type = null;
		 List<geometry> list_geometry = null;
		 List<latlng> list_location = null;
		 List<latlng> list_southwest = null;
		 List<latlng> list_northeast = null;
		 List<address_component> list_addr = null;
		 List<type> list_Type = null;
		List<latlng> list_southwest_v = null;
		List<latlng> list_northeast_v = null;
		List<latlng> list_southwest_b = null;
		List<latlng> list_northeast_b = null;
		List<sn> list_viewport = null;
		List<sn> list_bounds = null;
		type t = null;
		type T = null;
		geometry geo = null;
		address_component addr = null;
		latlng l = null;
		latlng s = null;
		latlng n = null;
		latlng vs = null;
		latlng vn = null;
		latlng bs = null;
		latlng bn = null;
		sn view = null;
		sn bound = null;
		 boolean tag_addr = false;
		 boolean tag_location = false;
		 boolean tag_southwest = false;
		 boolean tag_northeast = false;
		 boolean tag_view = false;
		 boolean tag_bound = false;
		 map m=new map();
		URL url;
		try {
			url = new URL(input.toString());
			InputStream is = url.openStream();
			XmlPullParser parser;
			parser = XmlPullParserFactory.newInstance().newPullParser();
			parser.setInput(is, null);
			int eventType = parser.getEventType();			
			while (eventType != XmlPullParser.END_DOCUMENT) {
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:
					list_map = new ArrayList<map>();
					break;
				case XmlPullParser.START_TAG:				
					if (parser.getName().equals("result")) {
						list_addr = new ArrayList<address_component>();
						list_geometry = new ArrayList<geometry>();
						m = new map();
						list_Type = new ArrayList<type>();
					}		
					if (parser.getName().equals("type")) {					
						if (tag_addr == true) {
							t = new type();
							t.setType(parser.nextText());
						} else if (tag_addr == false) {
							T = new type();
							T.setType(parser.nextText());
						}
					}
					if (parser.getName().equals("address_component")) {
						list_type = new ArrayList<type>();
						addr = new address_component();
						tag_addr = true;
					}
					if (parser.getName().equals("long_name")) {
						if (tag_addr == true) {
							addr.setLong_name(parser.nextText());
						}
					}
					if (parser.getName().equals("short_name")) {			
						if (tag_addr == true) {
							addr.setShort_name(parser.nextText());
						}
					}
					if (parser.getName().equals("geometry")) {				
						geo = new geometry();
						list_bounds= new ArrayList<sn>();
						list_viewport= new ArrayList<sn>();				
					}
					if (parser.getName().equals("location")) {
						tag_location = true;
						list_location = new ArrayList<latlng>();
						l = new latlng();			
					}
					if (parser.getName().equals("lat")) {										
						if (tag_view) {
							if (tag_southwest)
								vs.setLat(parser.nextText());
							if (tag_northeast)
								vn.setLat(parser.nextText());
						} else if (tag_bound) {
							if (tag_southwest)
								bs.setLat(parser.nextText());
							if (tag_northeast)
								bn.setLat(parser.nextText());
						} else if (tag_location) {
							l.setLat(parser.nextText());
						}				
					}
					if (parser.getName().equals("lng")) {
						if (tag_view) {
							if (tag_southwest)
								vs.setLng(parser.nextText());
							if (tag_northeast)
								vn.setLng(parser.nextText());
						} else if (tag_bound) {
							if (tag_southwest)
								bs.setLng(parser.nextText());
							if (tag_northeast)
								bn.setLng(parser.nextText());
						} else if (tag_location) {
							l.setLng(parser.nextText());
						}
					}			
					if (parser.getName().equals("viewport")) {					
						tag_view=true;
						view=new sn();
					}
					if (parser.getName().equals("bounds")) {
						tag_bound=true;						
						bound=new sn();
					}
					if (parser.getName().equals("southwest")) {
						tag_southwest = true;					
						if (tag_view) {
							list_southwest_v=new ArrayList<latlng>();
							vs = new latlng();
						}
						if (tag_bound) {
							list_southwest_b=new ArrayList<latlng>();
							bs = new latlng();						
						}
					}
					if (parser.getName().equals("formatted_address")) {
						m.setFormatted_address(parser.nextText());
					}
					if (parser.getName().equals("northeast")) {
						tag_northeast = true;				
						if (tag_view) {
							list_northeast_v=new ArrayList<latlng>();					
							vn = new latlng();
						}
						if (tag_bound) {
							list_northeast_b=new ArrayList<latlng>();											
							bn = new latlng();
						}
					}
					if (parser.getName().equals("location_type")) {
						geo.setLocation_type(parser.nextText());
					}			
					break;			
				case XmlPullParser.TEXT:
					break;
				case XmlPullParser.END_TAG:				
					if (parser.getName().equals("result")) {
						m.setAddr(list_addr);
						m.setGeo(list_geometry);
						m.setType(list_Type);
						list_map.add(m);
						m = null;
					}			
					if (parser.getName().equals("address_component")) {
						addr.setType(list_type);
						list_addr.add(addr);
						addr = null;
						tag_addr = false;
					}
					if (parser.getName().equals("type")) {
						if (tag_addr) {
							list_type.add(t);
							t = null;
						} else {
							list_Type.add(T);
							T = null;
						}
					}
					if (parser.getName().equals("geometry")) {
						geo.setLocation(list_location);
						geo.setBounds(list_bounds);
						geo.setViewport(list_viewport);					
						list_geometry.add(geo);
						geo=null;
					}
					if (parser.getName().equals("viewport")) {
						list_viewport.add(view);					
						tag_view=false;
						view=null;
					}
					if (parser.getName().equals("bounds")) {
						list_bounds.add(bound);
						tag_bound=false;
					}
					if (parser.getName().equals("southwest")) {
						if (tag_view) {
							list_southwest_v.add(vs);
							view.setSouthwest(list_southwest_v);						
						}
						if (tag_bound) {
							list_southwest_b.add(bs);
							bound.setSouthwest(list_southwest_b);
						}
						tag_southwest = false;
					}
					if (parser.getName().equals("northeast")) {
						if (tag_view) {
							list_northeast_v.add(vn);
							view.setNortheast(list_northeast_v);
						}
						if (tag_bound) {
							list_northeast_b.add(bn);
							bound.setNortheast(list_northeast_b);
						}
						tag_northeast = false;
					}
					if (parser.getName().equals("location")) {
						list_location.add(l);
						tag_location = false;
						l=null;
					}			
					break;
				}				
				eventType = parser.next();
			}
		} catch (IOException e) {			
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
		return list_map;
	}
}
