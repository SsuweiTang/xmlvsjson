package com.example.xmlvsjson;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.example.lib.address_component;
import com.example.lib.geometry;
import com.example.lib.latlng;
import com.example.lib.map;
import com.example.lib.sn;
import com.example.lib.type;

public class SAX {
	public SAX() {
	}

	// 要解析的XML文件以流的方式傳進來
	public List<map> getParser(String path) {
		MapsHanler mapHanler = null;
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		SAXParser saxParser = null;
		try {

			URL url = new URL(path.toString());
			InputStream is;
			is = url.openStream();
			saxParser = saxParserFactory.newSAXParser();
			mapHanler = new MapsHanler();
			saxParser.parse(is, mapHanler);

		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// 當解析過程中遇到符合XML語法規定的語句時就會調用第2個参數所規定的方法
		return mapHanler.getParser2();
	}

	class MapsHanler extends DefaultHandler {
		private List<map> list_map = null;
		private List<type> list_type = null;
		private List<geometry> list_geometry = null;
		private List<latlng> list_location = null;
		List<latlng> list_southwest_v = null;
		List<latlng> list_northeast_v = null;
		List<latlng> list_southwest_b = null;
		List<latlng> list_northeast_b = null;
		private List<sn> list_viewport = null;
		private List<sn> list_bounds = null;
		private List<address_component> list_addr = null;
		private List<type> list_Type = null;
		private type t = null;
		private type T = null;
		private geometry geo = null;
		private address_component addr = null;
		private latlng l = null;
		private latlng vs = null;
		private latlng vn = null;
		private latlng bs = null;
		private latlng bn = null;
		private sn view = null;
		private sn bound = null;
		private boolean tag_view = false;
		private boolean tag_bound = false;
		private boolean tag_addr = false;
		private boolean tag_location = false;
		private boolean tag_southwest = false;
		private boolean tag_northeast = false;

		private map m = null;
		private String TAG;// 存放當前所遍曆的元素節點
		private int i = 0;
		private String str_json = null;

		public MapsHanler() {
		}
		public void characters(char[] ch, int start, int length)
				throws SAXException {
			super.characters(ch, start, length);
			if (TAG != null) {
				String data = new String(ch, start, length);
				if ("type".equals(TAG)) {
					if (tag_addr == true) {
						t.setType(data);
					} else if (tag_addr == false) {
						T.setType(data);
					}
				}
				if ("formatted_address".equals(TAG)) {
					m.setFormatted_address(data);
				}
				if ("long_name".equals(TAG)) {
					if (tag_addr == true) {
						addr.setLong_name(data);
					}
				}
				if ("short_name".equals(TAG)) {

					if (tag_addr == true) {
						addr.setShort_name(data);
					}
				}
				if ("location_type".equals(TAG)) {
					geo.setLocation_type(data);

				}
				if ("lat".equals(TAG)) {

					if (tag_view) {
						if (tag_southwest)
							vs.setLat(data);
						if (tag_northeast)
							vn.setLat(data);
					} else if (tag_bound) {
						if (tag_southwest)
							bs.setLat(data);
						if (tag_northeast)
							bn.setLat(data);
					} else if (tag_location) {
						l.setLat(data);
					}
				}
				if ("lng".equals(TAG)) {

					if (tag_view) {
						if (tag_southwest)
							vs.setLng(data);
						if (tag_northeast)
							vn.setLng(data);
					} else if (tag_bound) {
						if (tag_southwest)
							bs.setLng(data);
						if (tag_northeast)
							bn.setLng(data);

					} else if (tag_location) {
						l.setLng(data);

					}
				}

			}
		}
		public void endDocument() throws SAXException {
			super.endDocument();
		}
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			super.endElement(uri, localName, qName);
			if ("result".equals(localName)) {
				m.setAddr(list_addr);
				m.setGeo(list_geometry);
				m.setType(list_Type);
				list_map.add(m);
				m = null;
			}
			if ("address_component".equals(localName)) {
				addr.setType(list_type);
				list_addr.add(addr);
				addr = null;
				tag_addr = false;
			}
			if (localName.equals("type")) {
				if (tag_addr) {
					list_type.add(t);
					t = null;
				} else {
					list_Type.add(T);
					T = null;
				}
			}
			if (localName.equals("geometry")) {
				geo.setLocation(list_location);
				geo.setBounds(list_bounds);
				geo.setViewport(list_viewport);
				list_geometry.add(geo);
				geo = null;
			}
			if (localName.equals("viewport")) {
				list_viewport.add(view);
				tag_view = false;
			}
			if (localName.equals("bounds")) {
				list_bounds.add(bound);
				tag_bound = false;
			}
			if (localName.equals("southwest")) {
				if (tag_view) {
					list_southwest_v.add(vs);
					view.setSouthwest(list_southwest_v);
				}
				if (tag_bound) {
					list_southwest_b.add(bs);
					bound.setSouthwest(list_southwest_b);
				}
				tag_southwest = false;
			}
			if (localName.equals("northeast")) {
				if (tag_view) {
					list_northeast_v.add(vn);
					view.setNortheast(list_northeast_v);
				}
				if (tag_bound) {
					list_northeast_b.add(bn);
					bound.setNortheast(list_northeast_b);
				}
				tag_northeast = false;
			}
			if (localName.equals("location")) {
				list_location.add(l);
				tag_location = false;
				l = null;
			}
			TAG = null;
		}
		public void startDocument() throws SAXException {
			super.startDocument();
			list_map = new ArrayList<map>();
		}
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			super.startElement(uri, localName, qName, attributes);
			if (localName.equals("result")) {
				list_addr = new ArrayList<address_component>();
				list_geometry = new ArrayList<geometry>();
				m = new map();
				list_Type = new ArrayList<type>();
			}
			if (localName.equals("address_component")) {
				list_type = new ArrayList<type>();
				addr = new address_component();
				tag_addr = true;
			}
			if (localName.equals("type")) {
				if (tag_addr) {
					t = new type();
				} else {
					T = new type();
				}
			}
			if (localName.equals("geometry")) {
				geo = new geometry();
				list_bounds = new ArrayList<sn>();
				list_viewport = new ArrayList<sn>();
			}

			if (localName.equals("southwest")) {
				tag_southwest = true;
				if (tag_view) {
					list_southwest_v = new ArrayList<latlng>();
					vs = new latlng();
				}
				if (tag_bound) {
					list_southwest_b = new ArrayList<latlng>();
					bs = new latlng();
				}
			}
			if (localName.equals("northeast")) {
				tag_northeast = true;
				if (tag_view) {
					list_northeast_v = new ArrayList<latlng>();
					vn = new latlng();
				}
				if (tag_bound) {
					list_northeast_b = new ArrayList<latlng>();
					bn = new latlng();
				}
			}
			if (localName.equals("location")) {
				tag_location = true;
				list_location = new ArrayList<latlng>();
				l = new latlng();
			}
			if (localName.equals("viewport")) {
				tag_view = true;
				view = new sn();
			}
			if (localName.equals("bounds")) {
				tag_bound = true;
				bound = new sn();
			}
			TAG = localName;
		}

		public List<map> getParser2() {
			return list_map;
		}
	}

}
