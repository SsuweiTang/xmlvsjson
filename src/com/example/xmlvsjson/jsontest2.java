package com.example.xmlvsjson;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class jsontest2 extends Activity{
	Thread threadjson = null;
	TextView tv_json=null;
	TextView tv_json_time=null;
	String str_json="";
	List<MapContent> list = new ArrayList<MapContent>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tv_json_time=(TextView)findViewById(R.id.textView1);
		tv_json=(TextView)findViewById(R.id.textView2);
		 
		xmlparser();
	}
	private void xmlparser() {
		// TODO Auto-generated method stub
		long beforeTime = System.currentTimeMillis();
		String path = "http://maps.google.com.tw/maps/api/geocode/xml?latlng=24.097305,120.6848459&sensor=true";
		try {
		URL url = new URL(path.toString());
		InputStream is;
	
			is = url.openStream();
		

		DocumentBuilderFactory factory = DocumentBuilderFactory
				.newInstance();

		DocumentBuilder builder= factory.newDocumentBuilder();
		Document document= builder.parse(is, "utf-8");
		Element element = document.getDocumentElement();

		NodeList nodes = element.getElementsByTagName("result");
		for (int i = 0; i < nodes.getLength(); i++) {
			Element subElement = (Element) nodes.item(i);
			str_json += "node"+i + "\n ";
			
			NodeList childNodes = subElement.getChildNodes();

			for (int j = 0; j < childNodes.getLength(); j++) {
				
				
				if (childNodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
					if ((childNodes.item(j).getNodeName()).equals("type")) {
						str_json += "   <type> "+ childNodes.item(j).getTextContent() + "\n ";
					}
					if ((childNodes.item(j).getNodeName()).equals("formatted_address")) {
						str_json += "   <formatted_address> "+ childNodes.item(j).getTextContent() + "\n ";
					}
//					if ((childNodes.item(j).getNodeName()).equals("address_component")) {
//						str_json += "   <address_component> "+ childNodes.item(j).getTextContent() + "\n ";
//					}
//					
					
					if((childNodes.item(j).getNodeName()).equals("address_component")) {
						NodeList childNodeschild = childNodes.item(j).getChildNodes();
						for(int k=0;k<childNodeschild.getLength();k++)
						{
//							System.out.println("node=="+j+"k=="+k);
							if (childNodeschild.item(k).getNodeType() == Node.ELEMENT_NODE) {
								if ((childNodeschild.item(k).getNodeName()).equals("long_name")) {
									str_json += "   <long_name> "+ childNodeschild.item(k).getTextContent() + "\n ";
											
								}
								if ((childNodeschild.item(k).getNodeName()).equals("short_name")) {
									str_json += "   <short_name> "+ childNodeschild.item(k).getTextContent() + "\n ";
										
								}
								if ((childNodeschild.item(k).getNodeName()).equals("type")) {
									str_json += "   <type> "+ childNodeschild.item(k).getTextContent() + "\n ";
											
								}
							}
							
						}
					}
					
					if((childNodes.item(j).getNodeName()).equals("geometry")) {
						NodeList childNodeschild = childNodes.item(j).getChildNodes();
						for(int k=0;k<childNodeschild.getLength();k++)
						{
							if ((childNodeschild.item(k).getNodeName()).equals("location")) {
								str_json += "   <location> \n ";
								
								NodeList subchilechildNodeschild = childNodeschild.item(k).getChildNodes();
								for(int l=0;l<subchilechildNodeschild.getLength();l++)
								{
									if ((subchilechildNodeschild.item(l).getNodeName()).equals("lat")) {
										str_json += "      <lat> "+ subchilechildNodeschild.item(l).getTextContent() + "\n ";
												
									}
									if ((subchilechildNodeschild.item(l).getNodeName()).equals("lng")) {
										str_json += "      <lng> "+ subchilechildNodeschild.item(l).getTextContent() + "\n ";
												
									}	
								}
										
							}
							if ((childNodeschild.item(k).getNodeName()).equals("location_type")) {
								str_json += "   <location_type> "+childNodeschild.item(k).getTextContent()+"\n ";
							}
							
							if ((childNodeschild.item(k).getNodeName()).equals("bounds")) {
								str_json += "   <bounds>\n ";
								NodeList chilechildNodeschild = childNodeschild.item(k).getChildNodes();	
								for(int p=0;p<chilechildNodeschild.getLength();p++)
								{
									if ((chilechildNodeschild.item(p).getNodeName()).equals("southwest")) {
										str_json += "       <southwest>\n ";
										NodeList subchilechildNodeschild = chilechildNodeschild.item(p).getChildNodes();
										for(int l=0;l<subchilechildNodeschild.getLength();l++)
										{
											if ((subchilechildNodeschild.item(l).getNodeName()).equals("lat")) {
												str_json += "          <lat> "+ subchilechildNodeschild.item(l).getTextContent() + "\n ";
														
											}
											if ((subchilechildNodeschild.item(l).getNodeName()).equals("lng")) {
												str_json += "          <lng> "+ subchilechildNodeschild.item(l).getTextContent() + "\n ";
														
											}	
										}
									}
									if ((chilechildNodeschild.item(p).getNodeName()).equals("northeast")) {
										
										str_json += "       <northeast>\n ";
										NodeList subchilechildNodeschild = chilechildNodeschild.item(p).getChildNodes();
										for(int l=0;l<subchilechildNodeschild.getLength();l++)
										{
											if ((subchilechildNodeschild.item(l).getNodeName()).equals("lat")) {
												str_json += "             <lat> "+ subchilechildNodeschild.item(l).getTextContent() + "\n ";
														
											}
											if ((subchilechildNodeschild.item(l).getNodeName()).equals("lng")) {
												str_json += "             <lng> "+ subchilechildNodeschild.item(l).getTextContent() + "\n ";
														
												}
											}
										}
									}
								}
							
							if ((childNodeschild.item(k).getNodeName()).equals("viewport")) {
								str_json += "   <viewport>\n ";
								NodeList chilechildNodeschild = childNodeschild.item(k).getChildNodes();	
								for(int p=0;p<chilechildNodeschild.getLength();p++)
								{
									if ((chilechildNodeschild.item(p).getNodeName()).equals("southwest")) {
										str_json += "           <southwest>\n ";
										NodeList subchilechildNodeschild = chilechildNodeschild.item(p).getChildNodes();
										for(int l=0;l<subchilechildNodeschild.getLength();l++)
										{
											if ((subchilechildNodeschild.item(l).getNodeName()).equals("lat")) {
												str_json += "          <lat> "+ subchilechildNodeschild.item(l).getTextContent() + "\n ";
														
											}
											if ((subchilechildNodeschild.item(l).getNodeName()).equals("lng")) {
												str_json += "          <lng> "+ subchilechildNodeschild.item(l).getTextContent() + "\n ";
														
											}	
										}
									}
									if ((chilechildNodeschild.item(p).getNodeName()).equals("northeast")) {
										
										str_json += "       <northeast>\n ";
										NodeList subchilechildNodeschild = chilechildNodeschild.item(p).getChildNodes();
										for(int l=0;l<subchilechildNodeschild.getLength();l++)
										{
											if ((subchilechildNodeschild.item(l).getNodeName()).equals("lat")) {
												str_json += "             <lat> "+ subchilechildNodeschild.item(l).getTextContent() + "\n ";
														
											}
											if ((subchilechildNodeschild.item(l).getNodeName()).equals("lng")) {
												str_json += "             <lng> "+ subchilechildNodeschild.item(l).getTextContent() + "\n ";
														
												}
											}
										}
									}
								}

							}

						}
					}
				}
			}
		
		long afterTime = System.currentTimeMillis();
		long parseTime = (afterTime-beforeTime);
		tv_json_time.setText("time"+parseTime);
		tv_json.setText(str_json);
		
		
		
		
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

}
