package com.example.xmlvsjson;

import java.util.ArrayList;
import java.util.List;

import com.example.lib.address_component;
import com.example.lib.geometry;
import com.example.lib.latlng;
import com.example.lib.map;
import com.example.lib.sn;
import com.example.lib.type;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

public class vs extends Activity {

	private TextView tv_json = null;
	private TextView tv_json_time = null;
	private TextView tv_sax = null;
	private TextView tv_sax_time = null;
	private TextView tv_dom = null;
	private TextView tv_dom_time = null;
	private TextView tv_pull = null;
	private TextView tv_pull_time = null;
	private String str_json = "";
	private String str_dom = "";
	private String str_sax = "";
	private String str_pull = "";
	long parseTime = 0;
	long parseTimeSAX = 0;
	long parseTimeDOM = 0;
	long parseTimePULL = 0;
	long parseTimeJSON = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vs);
		new pageStart().execute();

	}

	private class pageStart extends AsyncTask<Void, Integer, String> {

		// 背景工作處理"前"需作的事
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			findview();
		}

		@Override
		protected String doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			SAX();
			DOM();
			PULL();
			JSON();
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			tv_sax_time.setText("Parser time-> " + parseTimeSAX);
			tv_sax.setText(str_sax);
			tv_pull_time.setText("Parser time-> " + parseTimePULL);
			tv_pull.setText(str_pull);
			tv_dom_time.setText("Parser time-> " + parseTimeDOM);
			tv_dom.setText(str_dom);
			tv_json_time.setText("Parser time-> " + parseTimeJSON);
			tv_json.setText(str_json);

		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}
	}

	public void SAX() {
		long beforeTime = System.currentTimeMillis();
		String path = "http://maps.google.com.tw/maps/api/geocode/xml?latlng=24.097305,120.6848459&sensor=true";
		List<map> list_map = new ArrayList<map>();
		list_map = new SAX().getParser(path);
		long afterTime = System.currentTimeMillis();
		for (map m : list_map) {
			for (type t : m.getType()) {
				str_sax += "Type:" + t.getType() + "\n";
			}
			str_sax += "Faddress:" + m.getFormatted_address() + "\n";
			for (address_component a : m.getAddr()) {
				str_sax += "Long:" + a.getLong_name() + "\n";
				str_sax += "Short:" + a.getShort_name() + "\n";
				for (type t : a.getType()) {
					str_sax += "Type:" + t.getType() + "\n";
				}
			}
			for (geometry geo : m.getGeo()) {
				for (latlng l : geo.getLocation()) {
					str_sax += "Llat:" + l.getLat() + "\n";
					str_sax += "Llng:" + l.getLng() + "\n";
				}
				str_sax += "LTYPE:" + geo.getLocation_type() + "\n";
				for (sn view : geo.getViewport()) {
					for (latlng n : view.getNortheast()) {
						str_sax += "VNlat:" + n.getLat() + "\n";
						str_sax += "VNlng:" + n.getLng() + "\n";
					}
					for (latlng s : view.getSouthwest()) {
						str_sax += "VSlat:" + s.getLat() + "\n";
						str_sax += "VSlng:" + s.getLng() + "\n";
					}
				}
				if (geo.getBounds() != null) {
					for (sn bound : geo.getBounds()) {
						for (latlng n : bound.getNortheast()) {
							str_sax += "BNlat:" + n.getLat() + "\n";
							str_sax += "BNlng:" + n.getLng() + "\n";
						}
						for (latlng s : bound.getSouthwest()) {
							str_sax += "BSlat:" + s.getLat() + "\n";
							str_sax += "BSlng:" + s.getLng() + "\n";
						}
					}
				} else {
					System.out.println("geo.getBounds() is NULL");
				}
			}
			str_sax += "-------------" + "\n";
		}
		parseTimeSAX = (afterTime - beforeTime);
	}

	public void JSON() {
		long beforeTime = System.currentTimeMillis();
		String path = "http://maps.google.com.tw/maps/api/geocode/json?latlng=24.097305,120.6848459&sensor=true";
		List<map> list_map = new ArrayList<map>();
		list_map = new JSON().getParser(path);
		long afterTime = System.currentTimeMillis();
		for (map m : list_map) {
			for (type t : m.getType()) {
				str_json += "Type:" + t.getType() + "\n";
			}
			str_json += "Faddress:" + m.getFormatted_address() + "\n";
			for (address_component a : m.getAddr()) {
				str_json += "Long:" + a.getLong_name() + "\n";
				str_json += "Short:" + a.getShort_name() + "\n";
				for (type t : a.getType()) {
					str_json += "Type:" + t.getType() + "\n";
				}
			}
			for (geometry geo : m.getGeo()) {
				for (latlng l : geo.getLocation()) {
					str_json += "Llat:" + l.getLat() + "\n";
					str_json += "Llng:" + l.getLng() + "\n";
				}
				str_json += "LTYPE:" + geo.getLocation_type() + "\n";
				for (sn view : geo.getViewport()) {
					for (latlng n : view.getNortheast()) {
						str_json += "VNlat:" + n.getLat() + "\n";
						str_json += "VNlng:" + n.getLng() + "\n";
					}
					for (latlng s : view.getSouthwest()) {
						str_json += "VSlat:" + s.getLat() + "\n";
						str_json += "VSlng:" + s.getLng() + "\n";
					}
				}
				if (geo.getBounds() != null) {
					for (sn bound : geo.getBounds()) {
						for (latlng n : bound.getNortheast()) {
							str_json += "BNlat:" + n.getLat() + "\n";
							str_json += "BNlng:" + n.getLng() + "\n";
						}
						for (latlng s : bound.getSouthwest()) {
							str_json += "BSlat:" + s.getLat() + "\n";
							str_json += "BSlng:" + s.getLng() + "\n";
						}
					}
				} else {
					System.out.println("geo.getBounds() is NULL");
				}
			}
			str_json += "-------------" + "\n";
		}
		parseTimeJSON = (afterTime - beforeTime);
	}

	public void PULL() {
		long beforeTime = System.currentTimeMillis();
		String path = "http://maps.google.com.tw/maps/api/geocode/xml?latlng=24.097305,120.6848459&sensor=true";
		List<map> list_map = new ArrayList<map>();
		list_map = new SAX().getParser(path);
		long afterTime = System.currentTimeMillis();
		for (map m : list_map) {
			for (type t : m.getType()) {
				str_pull += "Type:" + t.getType() + "\n";
			}
			str_pull += "Faddress:" + m.getFormatted_address() + "\n";
			for (address_component a : m.getAddr()) {
				str_pull += "Long:" + a.getLong_name() + "\n";
				str_pull += "Short:" + a.getShort_name() + "\n";
				for (type t : a.getType()) {
					str_pull += "Type:" + t.getType() + "\n";
				}
			}
			for (geometry geo : m.getGeo()) {
				for (latlng l : geo.getLocation()) {
					str_pull += "Llat:" + l.getLat() + "\n";
					str_pull += "Llng:" + l.getLng() + "\n";
				}
				str_pull += "LTYPE:" + geo.getLocation_type() + "\n";
				for (sn view : geo.getViewport()) {
					for (latlng n : view.getNortheast()) {
						str_pull += "VNlat:" + n.getLat() + "\n";
						str_pull += "VNlng:" + n.getLng() + "\n";
					}
					for (latlng s : view.getSouthwest()) {
						str_pull += "VSlat:" + s.getLat() + "\n";
						str_pull += "VSlng:" + s.getLng() + "\n";
					}
				}
				if (geo.getBounds() != null) {
					for (sn bound : geo.getBounds()) {
						for (latlng n : bound.getNortheast()) {
							str_pull += "BNlat:" + n.getLat() + "\n";
							str_pull += "BNlng:" + n.getLng() + "\n";
						}
						for (latlng s : bound.getSouthwest()) {
							str_pull += "BSlat:" + s.getLat() + "\n";
							str_pull += "BSlng:" + s.getLng() + "\n";
						}
					}
				} else {
					System.out.println("geo.getBounds() is NULL");
				}
			}

			str_pull += "-------------" + "\n";
		}
		parseTimePULL = (afterTime - beforeTime);
	}

	public void DOM() {
		long beforeTime = System.currentTimeMillis();
		String path = "http://maps.google.com.tw/maps/api/geocode/xml?latlng=24.097305,120.6848459&sensor=true";
		List<map> list_map = new ArrayList<map>();
		list_map = new SAX().getParser(path);
		long afterTime = System.currentTimeMillis();
		for (map m : list_map) {
			for (type t : m.getType()) {
				str_dom += "Type:" + t.getType() + "\n";
			}
			str_dom += "Faddress:" + m.getFormatted_address() + "\n";
			for (address_component a : m.getAddr()) {
				str_dom += "Long:" + a.getLong_name() + "\n";
				str_dom += "Short:" + a.getShort_name() + "\n";
				for (type t : a.getType()) {
					str_dom += "Type:" + t.getType() + "\n";
				}
			}
			for (geometry geo : m.getGeo()) {
				for (latlng l : geo.getLocation()) {
					str_dom += "Llat:" + l.getLat() + "\n";
					str_dom += "Llng:" + l.getLng() + "\n";
				}
				str_dom += "LTYPE:" + geo.getLocation_type() + "\n";
				for (sn view : geo.getViewport()) {
					for (latlng n : view.getNortheast()) {
						str_dom += "VNlat:" + n.getLat() + "\n";
						str_dom += "VNlng:" + n.getLng() + "\n";
					}
					for (latlng s : view.getSouthwest()) {
						str_dom += "VSlat:" + s.getLat() + "\n";
						str_dom += "VSlng:" + s.getLng() + "\n";
					}
				}
				if (geo.getBounds() != null) {
					for (sn bound : geo.getBounds()) {
						for (latlng n : bound.getNortheast()) {
							str_dom += "BNlat:" + n.getLat() + "\n";
							str_dom += "BNlng:" + n.getLng() + "\n";
						}
						for (latlng s : bound.getSouthwest()) {
							str_dom += "BSlat:" + s.getLat() + "\n";
							str_dom += "BSlng:" + s.getLng() + "\n";
						}
					}
				} else {
					System.out.println("geo.getBounds() is NULL");
				}
			}
			str_dom += "-------------" + "\n";
		}
		parseTimeDOM = (afterTime - beforeTime);
	}

	private void findview() {
		// TODO Auto-generated method stub
		tv_json_time = (TextView) findViewById(R.id.json_tv1);
		tv_json = (TextView) findViewById(R.id.json_tv2);
		tv_sax_time = (TextView) findViewById(R.id.xml_sax_tv1);
		tv_sax = (TextView) findViewById(R.id.xml_sax_tv2);
		tv_pull_time = (TextView) findViewById(R.id.xml_pull_tv1);
		tv_pull = (TextView) findViewById(R.id.xml_pull_tv2);
		tv_dom_time = (TextView) findViewById(R.id.xml_dom_tv1);
		tv_dom = (TextView) findViewById(R.id.xml_dom_tv2);
	}
}