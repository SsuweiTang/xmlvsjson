package com.example.xmlvsjson;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;




import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class xmlParserPull extends Activity{
	Thread threadjson = null;
	TextView tv_json=null;
	TextView tv_json_time=null;
	String str_json="";
	List<MapContent> list = new ArrayList<MapContent>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tv_json_time=(TextView)findViewById(R.id.textView1);
		tv_json=(TextView)findViewById(R.id.textView2);
		 
		xmlparser();
	}
	private void xmlparser() {
		// TODO Auto-generated method stub
		long beforeTime = System.currentTimeMillis();
		String path = "http://maps.google.com.tw/maps/api/geocode/xml?latlng=24.097305,120.6848459&sensor=true";
		try {
			URL url = new URL(path.toString());
			InputStream is;
			is = url.openStream();
			XmlPullParser parser;
			parser = XmlPullParserFactory.newInstance().newPullParser();
			parser.setInput(is, null);
			int eventType = parser.getEventType();
				
				
				while (eventType != XmlPullParser.END_DOCUMENT) {
					switch (eventType) {

					case XmlPullParser.START_DOCUMENT:

						break;

					case XmlPullParser.START_TAG:
				
//						System.out.println("START_TAG"+parser.getName());
					if (parser.getName().equals("type")) {
						str_json += " <type>" + parser.nextText() + "\n ";

					}
					if (parser.getName().equals("address_component")) {
						str_json += " <address_component>" + "\n ";

					}
					if (parser.getName().equals("long_name")) {
						str_json += "      <long_name>" + parser.nextText()
								+ "\n ";

					}

					if (parser.getName().equals("short_name")) {
						str_json += "      <short_name>" + parser.nextText()
								+ "\n ";

					}
					if (parser.getName().equals("geometry")) {
						str_json += " <geometry>" + "\n ";

					}
					if (parser.getName().equals("location")) {
						str_json += " <location>" + "\n ";

					}
					if (parser.getName().equals("lat")) {
						str_json += " <lat>" + parser.nextText() + "\n ";

					}
					if (parser.getName().equals("lng")) {
						str_json += " <lng>" + parser.nextText() + "\n ";

					}
					if (parser.getName().equals("viewport")) {
						str_json += " <viewport>" + "\n ";

					}
					if (parser.getName().equals("viewport")) {
						str_json += " <viewport>" + "\n ";

					}
					if (parser.getName().equals("southwest")) {
						str_json += " <southwest>" + "\n ";

					}
					if (parser.getName().equals("formatted_address")) {
						str_json += " <formatted_address>" +parser.nextText() + "\n ";

					}
					if (parser.getName().equals("northeast")) {
						str_json += " <northeast>" + "\n ";

					}
					if (parser.getName().equals("location_type")) {
						str_json += " <location_type>" + parser.nextText() +"\n ";

					}
					
					
					
//						if(parser.getName().equals("id"))
//						{										
//							channel.setId(parser.nextText());
//						}
//						if(parser.getName().equals("ei_name"))
//						{					
//							channel.setEiname(parser.nextText());
//						}
//						

						break;
						
					case XmlPullParser.TEXT:

//						System.out.println("TEXT"+parser.getText());
//						System.out.println("parser"+parser.nextText());

						break;
						
						
						
					case XmlPullParser.END_TAG:
					
						break;
					}
					
					eventType = parser.next();
				}
			

		long afterTime = System.currentTimeMillis();
		long parseTime = (afterTime-beforeTime);
		tv_json_time.setText("time"+parseTime);
		tv_json.setText(str_json);
		
		
		
		
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

}

