package com.example.xmlvsjson;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class xmlParserSAX extends Activity {
	Thread threadjson = null;
	TextView tv_json = null;
	TextView tv_json_time = null;
	String str_json = "";
	List<MapContent> list = new ArrayList<MapContent>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tv_json_time = (TextView) findViewById(R.id.textView1);
		tv_json = (TextView) findViewById(R.id.textView2);

		xmlparser();
	}

	private void xmlparser() {
		// TODO Auto-generated method stub
		long beforeTime = System.currentTimeMillis();
		String path = "http://maps.google.com.tw/maps/api/geocode/xml?latlng=24.097305,120.6848459&sensor=true";
		try {

			MapsHanler mapHanler = null;
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = null;
			URL url = new URL(path.toString());
			InputStream is;
			is = url.openStream();
			saxParser = saxParserFactory.newSAXParser();
			mapHanler = new MapsHanler();
			saxParser.parse(is, mapHanler);

			long afterTime = System.currentTimeMillis();
			long parseTime = (afterTime - beforeTime);
			tv_json_time.setText("time" + parseTime);
			tv_json.setText(str_json);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	class MapsHanler extends DefaultHandler {

		private String TAG;// 存放當前所遍曆的元素節點
		private int i=0;
		public MapsHanler() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void characters(char[] ch, int start, int length)
				throws SAXException {
			super.characters(ch, start, length);
			if (TAG != null) {
				String data = new String(ch, start, length);

				if ("type".equals(TAG)) {
					str_json += " <type>" + data + "\n ";
				}
				if ("formatted_address".equals(TAG)) {
					str_json += " <formatted_address>" + data + "\n ";
				}
				if ("long_name".equals(TAG)) {
					str_json += " <long_name>" + data + "\n ";
				}
				if ("short_name".equals(TAG)) {
					str_json += " <short_name>" + data + "\n ";
				}
				if ("location_type".equals(TAG)) {
					str_json += " <location_type>" + data + "\n ";
				}
				if ("lat".equals(TAG)) {
					str_json += " <lat>" + data + "\n ";
				}
				if ("lng".equals(TAG)) {
					str_json += " <lng>" + data + "\n ";
				}
			}
		}

		@Override
		public void endDocument() throws SAXException {
			super.endDocument();
		}

		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			// System.out.println("endElement()");
			super.endElement(uri, localName, qName);
			if ("result".equals(localName)) {
				// list_channel.add(channel);
				//
				// channel=null;
			}

			// System.out.println("qName"+qName);
			TAG = null;

		}

		// 進行一些初始化的工作
		@Override
		public void startDocument() throws SAXException {
			super.startDocument();
			// System.out.println("startDocument()");
			// list_channel = new ArrayList<Channel>();
		}

		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			super.startElement(uri, localName, qName, attributes);
			// localName:不含命名空間前綴的標簽名
			// qName:含有命名空間前綴的標簽名
			// attributes:接收屬性值
			
			if (localName.equals("result")) {
				
				i++;
				str_json += "node" + i +"\n\n";
				// channel = new Channel();
				//

			}

			TAG = localName;

		}

	}

}
